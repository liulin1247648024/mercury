# -*- coding: utf-8 -*-

from pymongo.errors import DuplicateKeyError

from mercury.settings import logger
from mercury.monitor import cnt_dup_data


class Model:

    @classmethod
    async def get_by_nid(cls):
        """通过nid从数据库中查询新闻，并返回 News 对象
        """
        pass

    async def load_to_cache(self):
        """将新闻linkmd5加载入缓存

        ps. 暂时写这里，之后考虑移到专门的缓存管理模块
        """
        pass

    @property
    def uniqid(self):
        return getattr(self, self._uniqid)

    def to_mongo(self):
        """返回实例在 MongoDB 中的存储数据结构
        """
        if self._id is None:
            del self._id
        elif isinstance(self._id, str):
            self._id = ObjectId(self._id)
        data = self.__dict__.copy()
        return data

    async def update(self, data=None):
        if not data:
            data = self.to_mongo()
        clsname = type(self).__name__
        logger.info("[DB]<{0}> updating: {1}".format(clsname, data))
        retval = await self._db.update_one({'_id': self._id}, {'$set': data})
        logger.debug("[DB]<{0}> updated: {1}".format(clsname, self._id))
        return data

    async def save(self):
        clsname = type(self).__name__
        data = self.to_mongo()
        try:
            logger.info("[DB]<{0}> inserting: {1}".format(
                clsname, data["url"]))
            result = await self._db.insert_one(data)
            self._id = result.inserted_id
        except DuplicateKeyError as e:
            cnt_dup_data.labels(getway=self.getway).inc()
            logger.info("[DB]<{0}> already exists {1} {2}".format(
                clsname, self.urlmd5, self.url))
            retval = e
        else:
            data["_id"] = str(self._id)
            retval = data
            logger.debug("[DB]<{0}> inserted: {1}".format(clsname, retval))
        return retval

    @classmethod
    async def get_by_uniqid(cls, uniqid):
        if not uniqid:
            return None
        clsname = cls.__name__
        logger.info("[DB]<{0}> finding: {1}".format(clsname, uniqid))
        data = await cls._db.find_one({cls._uniqid: uniqid})
        logger.debug("[DB]<{0}> finding: {1}".format(clsname, data))
        inst = cls(**data)
        return inst

    async def remove(self):
        pass

# -*- coding: utf-8 -*-

# import sys
# sys.path.insert(0, "/Users/ju/workspace/mercury")

import asyncio
import aioredis
import random

from mercury.settings import STORAGE, logger


async def _connect_redis_pool():
    conf = STORAGE["redis"]
    try:
        redis_pool = await aioredis.create_redis_pool(
            (conf["host"], conf["port"]),
            db=1,
            password=conf["password"],
            encoding="utf8",
            loop=None)
    except Exception as e:
        logger.error(e, exc_info=True)
    else:
        return redis_pool


async def _retry_until_success():
    pool = await _connect_redis_pool()
    while not pool:
        # 睡眠 1~5 秒后重试
        await asyncio.sleep(random.uniform(1, 5))
        logger.info("[Redis] Retry connection")
        pool = await _connect_redis_pool()
    return pool


loop = asyncio.get_event_loop()
redis_pool = loop.run_until_complete(_retry_until_success())


if __name__ == "__main__":

    async def save():
        await redis_pool.set("testkey", 3)
        return await redis_pool.get("testkey")

    val = loop.run_until_complete(save())
    print(val)


# -*- coding: utf-8 -*-

# import sys
# sys.path.insert(0, "/Users/ju/workspace/mercury")

import asyncio

from pymongo import ReadPreference
from motor.motor_asyncio import AsyncIOMotorClient

from mercury.utils import md5
from mercury.settings import STORAGE


async def _connect_mongdb():
    conf = STORAGE["mongo"]

    client = AsyncIOMotorClient(
        host=conf["nodes"],
        replicaSet=conf.get("replset"),
        connect=False,
        username=conf.get("username"),
        password=conf.get("password"),
        authSource=conf.get("dbname")
    )
    mgdb = client.get_database(
        conf['dbname'],
        read_preference=ReadPreference.SECONDARY_PREFERRED)
    return mgdb

loop = asyncio.get_event_loop()
mgdb = loop.run_until_complete(_connect_mongdb())


if __name__ == "__main__":

    async def test():
        inret = await mgdb.test.insert_one({"hello": "world"})
        print(inret)
        outret = await mgdb.test.find_one()
        print(outret)
    loop.run_until_complete(test())

#!/usr/bin/env python
# encoding: utf-8

import asyncio
import logging
import mimetypes
from pathlib import Path

import aiofiles
import aiobotocore
from aiobotocore.config import AioConfig

import sys
sys.path.insert(0, '/Users/ju/workspace/mercury')

from mercury.settings import STORAGE
from mercury.settings import logger


def _get_client(s3conf):
    session = aiobotocore.get_session()
    client = session.create_client(
        's3',
        endpoint_url=s3conf['endpoint'],
        config=AioConfig(signature_version='s3v4'),
        region_name=s3conf['region'],
        aws_secret_access_key=s3conf['secret'],
        aws_access_key_id=s3conf['key'])
    return client


async def upload_text(content, key,
                      s3conf=None, contenttype='text/plain'):
    """上传文本内容到s3

    :param content: 要上传的内容
    :type content: str

    :param key: 上传到s3 bucket 中的key
    :type key: str

    :param s3conf: 要使用的s3配置
    :type cfg: dict or None

    :param contenttype: mime类型
    :type contenttype: str

    :return: issuccess
    :rtype: bool or None
    """
    if not s3conf:
        s3conf = STORAGE['s3']
    client = _get_client(s3conf)
    async with client as s3:
        try:
            resp = await s3.put_object(
                Bucket=s3conf['bucket'], Key=key, Body=content,
                ACL='public-read', ContentType=contenttype)
        except asyncio.TimeoutError:
            logger.error("S3 upload text timeout.")
        else:
            return resp['ResponseMetadata']['HTTPStatusCode'] == 200


async def upload_file(origin, target=None, s3conf=None, contenttype=None):
    """上传文件到s3

    :param origin: 待上传文件的路径, eg, path/filename.zip 或待上传bytes内容
    :type content: str or bytes

    :param target: 上传到s3 bucket 中的路径(key)
    :type key: str or None

    :param cfg: 要使用的s3配置名称
    :type cfg: str

    :param contenttype: mime类型
    :type contenttype: str or None

    :return: issuccess
    :rtype: bool or None
    """
    # 存放要传输的数据
    data = None
    # 猜测类型用的文件名
    typedet = None

    if isinstance(origin, bytes):
        data = origin
        if not target:
            raise Exception("`target` is None.")
        typedet = target
    else:
        if not target:
            target = origin
        f = await aiofiles.open(origin, mode='rb')
        try:
            data = await f.read()
        finally:
            await f.close()
        typedet = origin

    if not data:
        raise Exception("No data to upload.")

    if not contenttype:
        contenttype, encoding = mimetypes.guess_type(typedet)
    # 自动检测不到mimetype, 默认为二进制文件
    if not contenttype:
        contenttype = 'application/octet-stream'

    if not s3conf:
        s3conf = STORAGE['s3']
    client = _get_client(s3conf)

    try:
        async with client as s3:
            resp = await s3.put_object(
                Bucket=s3conf['bucket'], Key=target, Body=data,
                ACL='public-read', ContentType=contenttype)
            del data
            return resp['ResponseMetadata']['HTTPStatusCode'] == 200
    except Exception as e:
        logging.error(e, exc_info=True)


async def download_file(origin, target=None, s3conf=None):
    """从s3下载文件

    :param origin: 待下载文件在s3上的路径(key)
    :type content: str

    :param target: 下载后存放于本地的路径+文件名
    :type key: str or None

    :param cfg: 要使用的s3配置名称
    :type cfg: str

    :return: issuccess
    :rtype: bool
    """
    if not target:
        target = origin

    p = Path(target)
    if p.is_file():
        raise FileExistsError("{} already exists!".format(target))
    else:
        p.parent.mkdir(parents=True, exist_ok=True)

    if not s3conf:
        s3conf = STORAGE['s3']
    client = _get_client(s3conf)
    async with client as s3:
        resp = await s3.get_object(
            Bucket=s3conf['bucket'], Key=origin)
        async with resp['Body'] as stream:
            async with aiofiles.open(target, mode='wb') as f:
                await f.write(await stream.read())
    return p.is_file()


if __name__ == '__main__':
    import asyncio
    loop = asyncio.get_event_loop()

    data = open('/Users/ju/workspace/test.data', 'rb').read()

    print("uploading file...")
    upok = loop.run_until_complete(
        upload_file(data,
                    'test/0601-1.data'))
    print("downloading file...")
    downok = loop.run_until_complete(
        download_file('test/0601-1.data',
                      '/Users/ju/workspace/down0601-1.data'))
    print(upok)
    print(downok)

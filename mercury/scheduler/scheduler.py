# -*- coding: utf-8 -*-

import asyncio
import logging
import random

from mercury.models import Seed, Job, JobQueue, News, Case, Trademark
from mercury.utils import timestamp
from mercury.spiders import (WeixinSpider,
                             DefaultSpider,
                             BaiduNewsSpider,
                             WSCourtSpider,
                             BaituTrademarkSpider)
from mercury.settings import (logger, MAX_JOBS,
                              SEED_INTERVAL, SPIDER_PRIORITIES)
from mercury.monitor import (jobs_in_redis,
                             data_amount,
                             seeds_amount,
                             zero_seeds_amount,
                             jobs_inprogress,
                             cnt_job_started)


spiders = {
    "weixin": WeixinSpider,
    "baidunews": BaiduNewsSpider,
    "wscourt": WSCourtSpider,
    "baitutrademark": BaituTrademarkSpider,
    "default": DefaultSpider
}


class Scheduler:
    """调度类
    """

    def __init__(self):
        pass

    async def _get_jobs(self, num=30):
        """批量从抓取队列获取抓取任务
        """
        pass

    @classmethod
    async def dispatch(cls, job):
        """将下载任务分发给对应的spider
        """
        # 分发任务前进行一次重复检查，若重复则不分发
        is_crawled = await JobQueue.pool.get(
            "job:crawled:{0}".format(job.uniqid))
        if is_crawled:
            logger.info("[Sched] DUPLICATE: job.urlmd5:{0}".format(job.uniqid))
            return None

        spider_name = job.spider_name
        Spider = spiders.get(spider_name) or DefaultSpider
        logger.info("[Sched]Dispatch job.urlmd5:{0} to spider: {1}"
                    .format(job.uniqid, spider_name))
        jobs_inprogress.labels(getway=spider_name).inc()
        cnt_job_started.labels(getway=spider_name).inc()
        await Spider(job).start()
        jobs_inprogress.labels(getway=spider_name).dec()

    @classmethod
    async def check_new_seeds(cls, interval=SEED_INTERVAL):
        """检查数据库中是否有新种子加入，如果有则将
        新种子载入下载队列

        :param int interval: 每隔 interval 秒检查一次
        """
        # (当前时间 - 调度间隔)) >= 上次调度时间
        # 且 状态为OK的种子
        # ps. MongoDB 中Date对象为毫秒级
        should_sched = ("(new Date(ISODate().getTime()"
                        "- this._sched_interval*1000)"
                        ">= this.last_sched_time)"
                        "&& this.status=='OK'")
        while True:
            # 在应该调度种子时，先随机sleep一段时间，
            # 一定程度避免多个节点同时发起mongo查询请求而调度过多种子
            await asyncio.sleep(random.uniform(interval, interval+20))

            # 队列中下载作业数大于3000时不调度新种子
            jobs_left = await JobQueue.total_len()
            if jobs_left > 3000:
                continue
            logger.info("[Sched]Checking new seeds ...")
            async for seed in Seed.find(
                    {"$where": should_sched},
                    # priority是第一优先级, 抓取次数少的第二优先，创建时间早的第三优先
                    sort=[("priority", -1), ("crawled_times", 1), ("create_time", 1)]):

                job = Job(url=seed.url,
                          urlmd5=seed.urlmd5,
                          seed_urlmd5=seed.urlmd5,
                          spider_name=seed.getway,
                          anti_cfg=seed.anti_cfg,
                          priority=SPIDER_PRIORITIES[seed.getway],
                          query=seed.query,
                          payload=seed.payload,
                          is_seed=True)
                logger.info("[Sched]Seed to queue, URL {0}, GETWAY {1}"
                            .format(seed.url, seed.getway))
                await JobQueue.rpush(job)
                seed.last_sched_time = timestamp()
                seed.crawled_times += 1
                await seed.update()

    @classmethod
    async def run(cls):
        while True:
            logger.info("[Sched]Getting jobs from queue")
            # 给种子检查、API等协程多点执行机会
            await asyncio.sleep(0.1)
            jobs = await asyncio.gather(
                *[JobQueue.blpop() for _ in range(MAX_JOBS)])
            jobs = list(filter(None, jobs))
            if jobs:
                # 分发任务至具体的 Spider
                await asyncio.gather(*[cls.dispatch(job) for job in jobs])
            else:
                logger.info("[Sched]No download job yet")

            # 记录数据整体抓取量
            # TODO: 事实上，当前数据库中的量由某个节点检测即可，无需每各节点都
            # 重复地执行相同的查库命令，随着集群规模扩大，反而给数据库增加了压力
            # 现阶段无配置中心，故而暂时采取生成随机数对比的办法，让集群中所有
            # 节点有10%的概率去记录状态，集群节点数量大于10时，理想情况下就一直
            # 有节点在进行记录
            # 后续引入配置中心组件，明确地指派某一节点做记录即可
            if random.randint(0, 9) == 1:
                await cls._record_status()

    @staticmethod
    async def _record_status():
        """recording prometheus metrics
        """
        jobs_in_redis.set(await JobQueue.total_len())

        data_amount.labels(getway="baidunews").set(await News._db.estimated_document_count())
        data_amount.labels(getway="wscourt").set(await Case._db.estimated_document_count())
        data_amount.labels(getway="baitutrademark").set(await Trademark._db.estimated_document_count())

        seeds_amount.labels(getway="baidunews").set(
            await Seed._db.count_documents({"getway": "baidunews"}))
        seeds_amount.labels(getway="wscourt").set(
            await Seed._db.count_documents({"getway": "wscourt"}))
        seeds_amount.labels(getway="baitutrademark").set(
            await Seed._db.count_documents({"getway": "baitutrademark"}))

        zero_seeds_amount.labels(getway="baidunews").set(
            await Seed._db.count_documents({"getway": "baidunews", "crawled_times": 0}))
        zero_seeds_amount.labels(getway="wscourt").set(
            await Seed._db.count_documents({"getway": "wscourt", "crawled_times": 0}))
        zero_seeds_amount.labels(getway="baitutrademark").set(
            await Seed._db.count_documents({"getway": "baitutrademark", "crawled_times": 0}))

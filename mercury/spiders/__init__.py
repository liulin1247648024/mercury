# -*- coding: utf-8 -*-

from ._base import DefaultSpider, Spider as BaseSpider
from .wxsg import WeixinSpider
from .wscourt import Spider as WSCourtSpider
from .baidunews import Spider as BaiduNewsSpider
from .baitutrademark import BaituTrademarkSpider

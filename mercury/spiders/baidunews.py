# -*- coding: utf-8 -*-

import re
import lxml
import logging
from datetime import datetime, timedelta, timezone

import newspaper
from goose3 import Goose
from goose3.text import StopWordsChinese
from bs4 import BeautifulSoup

from ._base import Spider as BaseSpider
from mercury.models import News
from mercury.utils import md5
from mercury.settings import logger
from mercury.exceptions import GooseEncodeError
from mercury.monitor import cnt_extract_fail


PUBLISHER = r'^(?P<publisher>\S+)'
YEAR_MONTH_DAY = r'^(?P<year>\d{4})年(?P<month>\d{2})月(?P<day>\d{2})日'
HOUR_MINUTE = r'^(?P<hour>\d{2}):(?P<minute>\d{2})$'
SHORT_TIME = r'^(?P<digit>\d+)(?P<hmtype>小时|分钟)前$'


PAT_PUBLISHER = re.compile(PUBLISHER)
PAT_YEAR_MONTH_DAY = re.compile(YEAR_MONTH_DAY)
PAT_HOUR_MINUTE = re.compile(HOUR_MINUTE)
PAT_SHORT_TIME = re.compile(SHORT_TIME)


class Spider(BaseSpider):
    name = "baidunews"

    Model = News

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": (" Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:60.0)"
                               " Gecko/20100101 Firefox/60.0")
            },
            "cookies": {},
            "sleep_range_before_request": None,
            "uniqid": "urlmd5",
            "save_html": False
        }
        return configs

    @staticmethod
    def _extract_by_newspaper(url, page):
        try:
            article = newspaper.Article(url, memoize_articles=False,
                                        language='zh', fetch_images=False)
            article.set_html(page)
            article.parse()
        except Exception as e:
            logger.error(e, exc_info=True)
        else:
            return {"title": article.title,
                    "content": article.text}

    @staticmethod
    def _extract_by_goose(page, encoding):
        g = Goose({"stopwords_class": StopWordsChinese,
                   "strict": False})
        article = None
        try:
            # goose 自行识别编码并处理
            article = g.extract(raw_html=page)
        except (UnicodeEncodeError, LookupError):
            # 若因编码出错，采用传递进来的编码值
            try:
                article = g.extract(
                    raw_html=page.encode(encoding, errors='ignore'))
            except Exception as e:
                logger.error(e, exc_info=True)
        except lxml.etree.ParserError:
            pass
        except Exception as e:
            logger.error(e, exc_info=True)

        if article and article.cleaned_text:
            return {"title": article.title,
                    "content": article.cleaned_text}

    def _extract_html_page(self, url, page, encoding):
        """extract title and content by given raw html stings
        :param str page: raw html strings
        :param str encoding: raw html char encoding
        :return dict result: main fields of news
        """
        # TODO: 深入检查 newspaper 的 parse 机制以防出现如下异常
        # concurrent.futures.process.BrokenProcessPool
        # 应该是它在解析时又派生了子进程，而ProcessPoolExecuter不支持
        data = self._extract_by_goose(page, encoding)
        if not data:
            data = self._extract_by_newspaper(url, page)
        return data

    async def handle_detail(self, page, encoding, extra_data=None):
        url = self.job.url

        if self.executor:
            data = await self.loop.run_in_executor(
                self.executor, self._extract_html_page, url, page, encoding)
        else:
            data = self._extract_html_page(url, page, encoding)

        if data and extra_data:
            publisher = extra_data.get('publisher')
            pubtime = extra_data.get('pubtime')
            data.update({
                "url": url,
                "getway": "baidunews",
                "publisher": publisher,
                "pubtime": datetime.fromtimestamp(
                    int(pubtime), tz=timezone.utc),
                "sid": self.job.seed_urlmd5
            })
        return data

    async def handle_index(self, page):
        detail_infos, next_info = [], {}

        soup = BeautifulSoup(page, 'lxml')
        pages = soup.select('div.result')

        for page in pages:
            cauthor = page.select('p.c-author')[0].get_text(strip=True)
            publisher, pubtime = self._extract_pubinfo(cauthor)
            link = page.select('h3 > a')[0].get('href')
            pubinfo = {'publisher': publisher, 'pubtime': pubtime}
            detail_infos.append({'url': link, 'extra_data': pubinfo})

        nav_page = soup.select('p#page > a.n')
        if nav_page:
            href = nav_page[-1].get('href')
            rsv_page = re.search(r'rsv_page=(-?\d+)', href)[1]
            if rsv_page == '1':
                pn_new = re.search(r'pn=(\d+)', href)[0]
                pn_old_ret = re.search(r'pn=(\d+)', self.job.url)
                if pn_old_ret:
                    pn_old = pn_old_ret[0]
                    url_next = self.job.url.replace(pn_old, pn_new)
                else:
                    url_next = self.job.url + '&' + pn_new
                next_info = {'url': url_next}
        del soup, page
        return (detail_infos, next_info)

    @staticmethod
    def _extract_pubinfo(pubinfo):
        """
        :param pubinfo: str, contains publisher and pubtime
        :rtype (publisher, pubtime)
        """

        publisher, pubtime = '', datetime.utcnow()

        split_args = re.split(r'\s+', pubinfo)
        split_len = len(split_args)
        if split_len == 3:
            publisher = Spider._extract_publisher(split_args[0])
            pubtime = Spider._extract_time(split_args[1], split_args[2])
        elif split_len == 2:
            if ':' in split_args[-1]:
                pubtime = Spider._extract_time(split_args[0], split_args[1])
            else:
                publisher = Spider._extract_publisher(split_args[0])
                pubtime = Spider._extract_time(None, None, split_args[1])
        elif split_len == 1:
            if PAT_YEAR_MONTH_DAY.match(split_args[-1]):
                pubtime = Spider._extract_time(split_args[0])
            elif PAT_SHORT_TIME.match(split_args[-1]):
                pubtime = Spider._extract_time(None, None, split_args[-1])

        pubtime = pubtime.astimezone(tz=timezone.utc)
        pubtime = int(pubtime.timestamp())
        return (publisher, pubtime)

    @staticmethod
    def _extract_publisher(pub_str):
        """
        extract publisher from pub_str
        """
        publisher = PAT_PUBLISHER.match(pub_str).group('publisher')
        return publisher

    @staticmethod
    def _extract_time(ymd_str, hm_str=None, st_str=None):
        """
        extract pubtime from date str
        """
        tz_utc_8 = timezone(timedelta(hours=8))
        if st_str:
            t = Spider._extract_st(st_str)
        else:
            year, month, day = Spider._extract_ymd(ymd_str)
            if hm_str:
                hour, minute = Spider._extract_hm(hm_str)
                t = datetime(year, month, day, hour, minute, tzinfo=tz_utc_8)
            else:
                t = datetime(year, month, day, tzinfo=tz_utc_8)

        return t

    @staticmethod
    def _extract_ymd(ymd_str):
        """
        extract year, month, day from ymd_str
        """
        ymd_ret = PAT_YEAR_MONTH_DAY.match(ymd_str)
        year = int(ymd_ret.group('year'))
        month = int(ymd_ret.group('month'))
        day = int(ymd_ret.group('day'))
        return (year, month, day)

    @staticmethod
    def _extract_hm(hm_str):
        """
        extract hour, minute from hm_str
        """
        hm_ret = PAT_HOUR_MINUTE.match(hm_str)
        hour = int(hm_ret.group('hour'))
        minute = int(hm_ret.group('minute'))
        return (hour, minute)

    @staticmethod
    def _extract_st(st_str):
        """
        extract short time format form st_str
        """
        tz_utc_8 = timezone(timedelta(hours=8))
        st_ret = PAT_SHORT_TIME.match(st_str)
        digit = int(st_ret.group('digit'))
        hmtype = st_ret.group('hmtype')
        if hmtype == '分钟':
            t = datetime.now(tz=tz_utc_8) - timedelta(seconds=digit * 60)
        elif hmtype == '小时':
            t = datetime.now(tz=tz_utc_8) - timedelta(hours=digit)
        return t

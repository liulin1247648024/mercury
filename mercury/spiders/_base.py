# -*- coding: utf-8 -*-

import os
import gzip
import asyncio
import logging
import random
from datetime import date
from concurrent.futures import ProcessPoolExecutor

from pymongo.errors import DuplicateKeyError

from mercury.downloader import AsyncHttpDownloader
from mercury.models import Job, JobQueue
from mercury.libs import s3
from mercury.libs.proxy import get_proxy
from mercury.utils import md5
from mercury.settings import logger, SPIDER_PRIORITIES
from mercury.monitor import (cnt_db_saved,
                             cnt_download_failures,
                             cnt_extract_fail,
                             time_on_download,
                             time_on_write_mongo,
                             time_on_upload_s3,
                             time_on_extract_detail,
                             time_on_extract_index)

cpu_num = os.cpu_count()


class Spider:

    loop = asyncio.get_event_loop()
    # 如果CPU核心数量大于 1 才用进程池
    executor = ProcessPoolExecutor(
        max_workers=cpu_num) if cpu_num > 1 else None
    queue = JobQueue.pool

    def __init__(self, job):
        self.job = job
        self.configs = {}
        self.configs.update(job.anti_cfg)
        self.configs.update(self.setup_configs())
        self.dl = None

    def setup_configs(self):
        """爬虫的配置，子类中只需返回个性化定制的配置项

        :supported config items:

        :config `headers` dict: custom HTTP request headers
        :config `sleep_range_before_request` tuple: (start, stop), sleep
            some time at top of `_make_request`
        """
        raise NotImplementedError("Must setup configurations for spider.")

    async def handle_index(self, page):
        """解析索引页(又称列表页)，能够获取文章详情链接的页面
        如，搜索返回的文章列表、网站子栏目里的文章列表

        返回需要抓取的所有详情链接，分页也在此处理

        :param page: 列表页面内容字符串
        :return: 详情链接列表与下一页网址 ([url1, url2, ...], nextpage_url)
            如果没有下一页，nextpage_url 值为None
        """
        raise NotImplementedError("Must implement how to handle index page.")

    async def handle_detail(self, page, encoding, extra_data=None):
        """解析文章详情页，返回相应的详情内容，字段见models中的模块
        :param page: 详情页面内容字符串
        :param encoding: 详情页面内容字符编码
        :param extra_data: index页面传递给detai页面的数据, 默认为None
        :return: 代表文章详情的字典
        """
        raise NotImplementedError("Must implement how to handle detail page.")

    async def _make_request(self):
        """构造合适的HTTP请求，并下载页面
        """
        sleep_range = self.configs.get("sleep_range_before_request")
        if sleep_range:
            await asyncio.sleep(random.uniform(*sleep_range))
        if self.configs.get("cookie"):
            should_close_cookie = True
        if self.configs.get("random_ua"):
            ua = "randomly select a User-Agent from somewhere"
        page, encoding = None, None
        if await self.before_request():
            dl = self.dl or AsyncHttpDownloader(
                headers=self.configs.get("headers"),
                cookies=self.configs.get("cookies"),
                proxy=get_proxy() if self.configs.get('proxy_enable') else None)
            page, encoding = await dl.fetch(
                self.job.url, method=self.job.method,
                ctype=self.job.ctype, data=self.job.payload,
                rtype=self.job.rtype, auto_close=self.job.auto_close)
        return page, encoding

    async def before_request(self):
        return True

    async def handle_dup_detail(self):
        pass

    async def _start_index(self, page, encoding):
        # 本次任务是列表页，则将列表页中解析出的
        # 文章详情 url 生成下载作业并推送到下载队列
        logger.info("[Spider]<{0}> handle index page of: {1}"
                    .format(self.name, self.job.url))

        ts_start = self.loop.time()
        detail_infos, next_info = await self.handle_index(page)
        ts_end = self.loop.time()
        time_on_extract_index.labels(
            getway=self.name).observe(ts_end - ts_start)

        if next_info:
            detail_infos.append(next_info)
            is_seed = True
        else:
            is_seed = False

        priority = SPIDER_PRIORITIES[self.name]
        while detail_infos:
            detail = detail_infos.pop()
            job = Job(url=detail.pop('url'),
                      seed_urlmd5=self.job.seed_urlmd5,
                      spider_name=self.job.spider_name,
                      anti_cfg=self.job.anti_cfg,
                      priority=priority,
                      is_seed=is_seed,
                      uniqid=detail.pop(self.configs.get('uniqid'), None),
                      **detail)
            await JobQueue.rpush(job)
            is_seed = False

    async def _start_detail(self, page, encoding):
        logger.info("[Spider]<{0}> handle detail page of: {1}"
                    .format(self.name, self.job.url))

        ts_start = self.loop.time()
        data = await self.handle_detail(page, encoding, self.job.extra_data)
        ts_end = self.loop.time()
        time_on_extract_detail.labels(
            getway=self.name).observe(ts_end - ts_start)

        # 页面有内容，但无目标数据视为抽取失败
        if page and (not data):
            cnt_extract_fail.labels(getway=self.name).inc()

        if data:
            url = data.pop('url', None) or self.job.url
            urlmd5 = self.job.urlmd5
            inst = self.Model(url, urlmd5, **data)
            ts_start = self.loop.time()
            retval = await inst.save()
            ts_end = self.loop.time()
            time_on_write_mongo.labels(
                getway=self.name).observe(ts_end - ts_start)

            if isinstance(retval, DuplicateKeyError):
                return None
            else:
                cnt_db_saved.labels(getway=self.name).inc()

            # 入库完成后标记本详情页面已经抓取过
            uniqid = self.job.uniqid
            await self.queue.set("job:crawled:{0}".format(uniqid), 1)

            if self.configs.get("save_html"):
                # 存原始HTML页面到S3服务
                fname = "{spider}/{date}/{urlmd5}.html.gz".format(
                    spider=self.name,
                    date=date.today().strftime("%Y%m%d"),
                    urlmd5=self.job.urlmd5)

                ts_start = self.loop.time()
                await s3.upload_file(gzip.compress(page.encode(encoding)), fname)
                ts_end = self.loop.time()
                time_on_upload_s3.labels(
                    getway=self.name).observe(ts_end - ts_start)
                logger.info("[Spider]<{0}> upload {1} to s3 finished"
                            .format(self.name, fname))

    async def start(self):
        logger.info("[Spider]<{0}> start crawling url: {1}"
                    .format(self.name, self.job.url))

        ts_start = self.loop.time()
        page, encoding = await self._make_request()
        ts_end = self.loop.time()
        time_on_download.labels(getway=self.name).observe(ts_end - ts_start)

        # page 里若无内容认为下载失败，做一次统计
        if not page:
            cnt_download_failures.labels(getway=self.name).inc()
            # Session未关闭但是Page为空, 无法继续处理, 关闭Session
            if not self.job.auto_close:
                await self.dl.close()
        else:
            if self.job.is_seed:
                await self._start_index(page, encoding)
            else:
                await self._start_detail(page, encoding)


class DefaultSpider(Spider):
    """默认 Spider，做成自动处理的
    """

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        return {}

    async def start(self):
        # 遇到未知类型的Job,把Job放回任务队列中
        await JobQueue.rpush(self.job)

    async def handle_index(self, resp):
        pass

    async def handle_detail(self, resp):
        pass

import re
import time

import asyncio
import async_timeout
import aiohttp
from bs4 import BeautifulSoup as bs
from goose3 import Goose
import socket
from goose3.text import StopWordsChinese
from ._base import Spider as BaseSpider


class WeixinSpider(BaseSpider):
    name = "weixin"

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": ("Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:60.0)"
                               " Gecko/20100101 Firefox/60.0")
            },
            "cookies": {}
        }
        return configs

    def handle_index(self, resp):
        """
        处理列表页的方法， resp是response对象， 返回所有的详情页链接
        """
        html = bs(resp,'lxml')
        labels_a = html.select("div.news-box > ul.news-list > li > div.txt-box > h3 > a")
        pages_link = html.select("div#pagebar_container > a")
        if pages_link[-1].get("uigs") == "page_next":
            url_next = pages_link[-1].get("href")
            page_new = re.search("page=\d{1,9}",url_next)[0]
            try:
                page_old = re.search("page=\d{1,9}",self.job.url)[0]
            except:
                url_next = self.job.url + "&" + page_new
            else:
                url_next = self.job.url.replace(pn_old, pn_new)
        else:
            url_next = None
        return ([label_a.get("href") for label_a in labels_a], url_next)

    def handle_detail(self, resp, article_time=None):
        """
        处理详情页的方法， 返回文章信息
        """
        g = Goose({"stopwords_class": StopWordsChinese,
                   "strict": False})
        article = g.extract(raw_html=resp)
        content = article.cleaned_text
        html = bs(resp,'lxml')
        title = html.select("div#img-content > h2#activity-name")
        author = html.select("div#img-content >  div#meta_content > span.rich_media_meta")
        source = html.select("div#img-content >  div#meta_content > span#profileBt > a")
        if article_time:
            art_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(article_time)))
        else:
            art_time = None
        if title:
            title = title[0].text.split('"')[-2]
        else:
            title = None
        if len(author) == 2:
            author = author[0].text
        elif len(author) == 3:
            author = author[1].text
        else:
            author = None
        if source:
            source = "".join(source[0].text.split())
        else:
            source = None
        data = {
            "url": self.job.url,
            "title": title,
            "author": author,
            "publisher": source,
            "time": art_time,
            "getway": "weixin",
            "content": content,
            "sid": self.job.seed_urlmd5
        }
        return data


#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import sys
sys.path.insert(0, os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))

import asyncio
import uvloop
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
from signal import signal, SIGINT

from sanic import Sanic
from sanic.response import json, text
from sanic.exceptions import NotFound

from prometheus_client import exposition

from mercury.libs import mongo
from mercury.api import seeds
from mercury.libs import redis
from mercury.scheduler.scheduler import Scheduler

app = Sanic(__name__)


@app.exception(NotFound)
def ignore_404s(request, exception):
    return json({"msg": "what are you looking for?"})


@app.get("/metrics")
async def metrics(request):
    output = exposition.generate_latest().decode("utf-8")
    return text(body=output,
                content_type=exposition.CONTENT_TYPE_LATEST)


app.blueprint(seeds.bp)


if __name__ == "__main__":

    server = app.create_server(
        host="0.0.0.0", port=8888, debug=False)

    loop = asyncio.get_event_loop()
    loop.create_task(server)
    loop.create_task(Scheduler.run())
    loop.create_task(Scheduler.check_new_seeds())
    signal(SIGINT, lambda s, f: loop.stop())
    try:
        loop.run_forever()
    except:
        loop.stop()

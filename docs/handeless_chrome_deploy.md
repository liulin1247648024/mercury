### handless chrome 模块部署

参考     
[https://github.com/bosondata/chrome-prerender](https://github.com/bosondata/chrome-prerender)   
[https://github.com/prerender/prerender#official-middleware](https://github.com/prerender/prerender#official-middleware)   

1. 安装最新版本的chrome浏览器. mac下应该已安装, linux下可以在这里下载 https://www.google.com/chrome/browser/beta.html?platform=linux

2. 为指定chrome环境变量名, 方便操作. mac下编辑 ~/.zshrc 或者 ~/.bash_profile, 将下面4行代码加入

```bash
alias chrome="/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome"
alias chrome-canary="/Applications/Google\ Chrome\ Canary.app/Contents/MacOS/Google\ Chrome\ Canary"
alias chromium="/Applications/Chromium.app/Contents/MacOS/Chromium"
alias google-chrome="chrome"
```

linux下找到chrome的安装路径替换即可。

3. 安装 prerender

```bash
$ pip3 install -U prerender
```

4. 安装 gunicorn

```bash
$ pip3 install -U gunicorn
```

5. 启动chrome-handless

```bash
$ google-chrome --headless --remote-debugging-port=9222 --disable-gpu --blink-settings=imagesEnabled=false "about:blank"
```

6. 启动 prerender

```
$ prerender
```

7. 启动 gunicorn

```bash
$ gunicorn --bind 0.0.0.0:3000 --worker-class sanic.worker.GunicornWorker prerender.app:app
```

8. 测试

向 ```[http://127.0.0.1:3000/](http://127.0.0.1:3000/)``` 发送要解析的url.

```bash
$ curl http://127.0.0.1:3000/http://wenshu.court.gov.cn/content/content?DocID=d8a7a36f-bea8-488a-9470-32cb70c2d002
```

可以在命令行看到由js渲染后的页面(js代码已经被移除).

转化为pdf

```bash
$ curl http://127.0.0.1:3000/pdf/http://wenshu.court.gov.cn/content/content?DocID=d8a7a36f-bea8-488a-9470-32cb70c2d002
```

转化为png

```bash
$ curl http://127.0.0.1:3000/png/http://wenshu.court.gov.cn/content/content?DocID=d8a7a36f-bea8-488a-9470-32cb70c2d002
```

转化为jpeg

```bash
$ curl http://127.0.0.1:3000/jpg/http://wenshu.court.gov.cn/content/content?DocID=d8a7a36f-bea8-488a-9470-32cb70c2d002
```


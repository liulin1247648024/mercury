### mercury 爬虫项目环境搭建 (Mac OS)

需要安装的软件或搭建的服务:

* docker # 容器
* redis # 内存数据库
* mongodb # 持久化存储数据库
* minio # 对象存储服务, 兼容亚马逊S3云存储服务接口

#### 安装docker

Docker 官网上下载 CE版本, 链接 [https://store.docker.com/editions/community/docker-ce-desktop-mac](https://store.docker.com/editions/community/docker-ce-desktop-mac), 选择Stable的版本。

安装之后需要添加一些配置信息。Performance >> Daemon >> Registry mirrors 添加 ```https://registry.docker-cn.com/``` 来加快访问速度。修改完成之后在命令行输入:

```bash
docker --info
```

查看安装的docker信息。

运行下面命令查看是否安装成功。

```bash
docker run -it hello-world
```

#### 安装Redis

基于Docker，redis的安装更加方便。运行以下命令即可。

```bash
docker run -d --name mredis -v /data/redis:/data -p 6379:6379 -e REDIS_PASSWORD=test123 redis sh -c 'exec redis-server --requirepass "$REDIS_PASSWORD"'
```

首先要在 根目录 ```/``` 下建立 ```/data``` 目录, 用来存放项目中产生的所有数据。然后修改 ```/data``` 目录的用户权限为普通用户而非root权限,否则每次向 ```/data``` 写东西都要运行 ```sudo``` 命令。

执行下面的命令。

```bash
chown -R liutaotao:staff /data
```

安装完毕后, 执行下面的命令进行测试。

```bash
docker exec -it mredis bash
redis-cli
```
成功进入redis交互界面说明安装成功。

设置访问redis的password

```redis
auth test123
```

退出redis, 退出docker。

进入本地的 redis 交互界面 (本地安装需要执行 ```brew install redis```)

```redis
CONFIG SET requirepass 'test123'
```

#### 安装mongodb

使用homebrew安装 mongodb。

```bash
brew install mongodb
```

首先为mongodb设置存储路径。

```bash
mkdir -p /data/mongo/local{0..2}
```

这样在 ```/data/mongo/``` 目录下创建了 ```local0 local1 local2``` 三个文件夹。

为这三个存储目录创建 keyfile 用来 后面的的文件夹提供通信。

openssl rand -base64 756 > keyfile

执行下面的命令使得 keyfile 为只读文件。

然后将这3个 keyfile 文件 分别拷贝到 ```local0 local1 local2``` 这三个文件夹下面。

可以使用下面的命令

```bash
for i in 0 1 2; do cp keyfile /data/mongo/local$i; done;
```

开启3个mongodb进程:

第一个:

```bash
mongod --replSet rs0 --port 27017 --bind_ip 0.0.0.0 --dbpath /data/mongo/local0 --keyFile /data/mongo/local0/keyfile --smallfiles --oplogSize 128 --fork --logpath /data/log/mongo/local0.log
```

第二个:

```bash
mongod --replSet rs0 --port 27018 --bind_ip 0.0.0.0 --dbpath /data/mongo/local1 --keyFile /data/mongo/local1/keyfile --smallfiles --oplogSize 128 --fork --logpath /data/log/mongo/local1.log
```

第三个:

```bash
mongod --replSet rs0 --port 27019 --bind_ip 0.0.0.0 --dbpath /data/mongo/local2 --keyFile /data/mongo/local2/keyfile --smallfiles --oplogSize 128 --fork --logpath /data/log/mongo/local2.log
```

进入到 mongodb 中。命令行中输入下面命令

```bash
mongo --port 27017 # mongo --port 27018 or mongo --port 27019
```

分别使用上面注册的端口号, 进入到mongodb交互界面中, 查看该端口号下的实例是不是PRIMARY节点。PRIMARY节点交互信息显示如下:

```bash
rs0:PRIMARY>
```

集群初始化:

```mongodb
rs.initiate( {
   _id : "rs0",
   members: [
      { _id: 0, host: "127.0.0.1:27017" },
      { _id: 1, host: "127.0.0.1:27018" },
      { _id: 2, host: "127.0.0.1:27019" }
   ]
})
```

设置管理员，用来管理其他用户。

```mongodb
use admin
db.auth('admin', 'admin123')
```

设置集群的管理员账号, 用户名和密码分别为 cadmin:C4dm1n! :

```mongodb
db.createUser(
  {
    user: "cadmin",
    pwd: "C4dm1n!",
    roles: [ { role: "clusterAdmin", db: "admin" } ]
  }
)
```

创建 mercury 仓库，用户名和密码

```mongodb
db.createUser(
  {
    user: "mercury",
    pwd: "mercury123",
    roles: [ { role: "readWrite", db: "mercury" } ]
  }
)
```

执行下面命令查看创建的用户信息

```mongodb
db.system.users.find().pretty()
```

为mercury仓库下的collection创建索引:

```mongodb
use mercury
db.auth('mercury', 'mercury123')
db.news.createIndex({"urlmd5": 1}, {"unique":1, "background":1})
db.news.createIndex({"create_time": -1}, {"background":1})
db.seeds.createIndex({"create_time": -1}, {"background":1})
db.seeds.createIndex({"urlmd5": 1}, {"unique":1, "background":1})
db.seeds.createIndex({"getway": 1}, {"background":1})
db.seeds.createIndex({"crawled_times": 1}, {"background":1})
db.seeds.createIndex({"priority": -1}, {"background":1})
db.cases.createIndex({"create_time": -1}, {"background":1})
db.cases.createIndex({"docid": 1}, {"unique":1, "background":1})
db.trademark.createIndex({"create_time": -1}, {"background":1})
db.trademark.createIndex({"guid": 1}, {"unique":1, "background":1})
```

执行下面命令查看创建索引后自动生成的collections。

```mongodb
show tables
```

#### 安装 minio

首先在 ```/data``` 目录下新建 ```/minio``` 文件夹作为minio的存储路径。

```bash
mkdir -p /data/minio
```

然后访问minio的官网 [https://minio.io/](https://minio.io/), 按照说明，执行下面的命令:

```docker
docker run -p 9000:9000 --name minio1 -v /data/minio:/data -v /mnt/config:/root/.minio minio/minio server /data
```

记录下 key 和 secret 值, 访问 [http://127.0.0.1:9000](http://127.0.0.1:9000), 输出上一步获取的key和secret值登入后即成功安装。

#### 克隆mercury仓库并运行

在本地新建一个名为metasota的文件夹，然后新建一个虚拟环境。

```bash
mkdir metasota & cd metasota
mkdir venvs
python -m venv venvs/mercury
```

启动虚拟环境

```bash
source venvs/mercury/bin/activate
```

在bitbuket上先fork一下mercury仓库, 然后克隆到本地。

```bash
git clone git@bitbucket.org:liutaotao/mercury.git
```

同步代码仓库分支

```bash
cd mercury
git fetch --rebase upstream develop
```

项目初始化:

```bash
make clean # 清除原有的或者本项目不需要的第三方包
make deps # 安装基础依赖包
make pip # 安装开发|生产|测试环境依赖包
```

```/data``` 目录下新建 ```/log``` 目录作为项目的日志文件存储路径。

最后执行

```bash
python mercury/run.py
```

如果项目正常运行, 则说明启动成功。

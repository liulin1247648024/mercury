"""为指定爬虫重建缓存"""

import sys
import asyncio

sys.path.insert(0, '../mercury')
from mercury.libs.mongo import mgdb
from mercury.libs.redis import redis_pool


async def load_crawled_jobs_to_cache(colname, uniqid):
    """从指定的集合中获取唯一索引, 写入Redis中
    : param colname: 指定集合名称
    : param uniquid: 集合唯一索引
    """
    collection = getattr(mgdb, colname)
    cursor = collection.find({}, {uniqid: 1, '_id': 0}
                             ).sort([('create_time', 1)])
    cursor.batch_size(4096)
    counter = 0
    async for doc in cursor:
        await redis_pool.set('job:crawled:{}'.format(doc[uniqid]), '1')
        counter += 1
        if counter % 100 == 0:
            print("{} items was inserted".format(counter))

    print('total inserted {} items'.format(counter))


def main(colname, uniqid):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(load_crawled_jobs_to_cache(colname, uniqid))


if __name__ == '__main__':
    # colname: 爬虫存储的集合名称
    # uniqid: 唯一索引
    # example: news urlmd5, cases docid, trademark guid
    colname, uniqid = sys.argv[1:]
    main(colname, uniqid)

# 白兔商标字段

### 字段名称含义

```python

FIELDS: {
    '_id': "",
    'url': 'url',
    'urlmd5': 'md5(url)', 
    'status': '商标状态',
    'notice_first_trial': '初审公告',
    'reg_code': '注册码',
    'inter_class': '国际分类',
    'name_zh': '中文名称',
    'name_en': '英文名称',
    'date_apply': '申请日期',
    'date_first_trial': '初审日期',
    'date_reg': '注册日期',
    'date_exp': '截止日期',
    'reg_person': '注册人',
    'reg_addr': '注册地址',
    'agency': '代理组织',
    'services': '使用商品',
    'features': [
        '图片特征1',
        '图片特征2'
    ],
    'url_image': '商标链接', 
    'reg_process': [
        ("date1", "商标申请流程1"),
        ("date2", "商标申请流程2"),
    ],
    'getway': '获取途径', 
    'sid': 'seed_id', 
    'guid': 'url中的查询参数guid',
    'create_time': '创建时间'
}
```

### for example:
```python

FIELDS: {
    '_id': '5b6189959810b437fba0f5ab',
	'url': 'http://www.cha-tm.cn/chatmbs/tmwx/baitusoft/sbcxwx/sbcxwxInfo.jsp?guid=1018954FE127D0D9F8',
	'urlmd5': '7026371adfd145688c2cf10491a51063',
	'status': '已注册',
	'notice_first_trial': '965@304',
	'reg_code': '3795790',
	'inter_class': '10',
	'name_zh': '湘仪',
	'name_en': '',
	'date_apply': '2003-11-12',
	'date_first_trial': '',
	'date_reg': '2005-05-28',
	'date_exp': '2025-05-27',
	'reg_person': '苏慧',
	'reg_addr': '湖南省望城县望城经济开发区金穗路35号',
	'agency': '北京金信联合知识产权代理有限公司',
	'services': '医疗器械和仪器,医疗器械箱,血压计,医用喷雾器,医用细菌培养箱,按摩器械,验血仪器,健美按摩设备,血红素计,复苏器',
	'features': ['1.1.8'],
	'url_image': 'http://images.cha-tm.cn/tmimages/10/2003/0x7900540020004A004900470054002700.jpg',
	'reg_process': [
	    ('2011-03-11', '商标注册申请中'),
	    ('2011-10-10', '打印驳回或部分驳回通知书'), 
	    ('2011-12-05', '注册申请部分驳回'), 
	    ('2011-12-07', '注册申请初步审定'), 
	    ('2011-12-15', '注册申请部分驳回'), 
	    ('2012-05-03', '商标已注册')
	    ],
	'getway': 'baitutrademark',
	'sid': '2969a7f59a54f216e172c124d1403db3',
	'guid': '1018954FE127D0D9F8',
	'create_time': 'datetime.datetime(2018, 8, 8, 7, 33, 55, 732593)'
}
```

# -*- coding: utf-8 -*-

import os
import requests

import demjson

DIR_SCRIPT = os.path.dirname(os.path.abspath(__file__))
category_path = os.path.join(DIR_SCRIPT, 'trademark_category.json')
feature_path = os.path.join(DIR_SCRIPT, 'trademark_feature.json')

INDEX_URL = "http://www.cha-tm.cn/chatmbs/tmwx/baitusoft/sbtxwx/sbtxwxList.jsp?c={category}&k={feature}"


# 提取图片特征
def extract_features():
    with open(feature_path, 'r') as ff:
        feature = ff.read()
    features = demjson.decode(feature)

    for feature in features:
        yield feature

# 提取国际分类
def extract_category():
    with open(category_path, 'r') as fc:
        categorys = fc.read()
        categorys = demjson.decode(categorys)
    for category in categorys:
        yield category

# 列表页 url 拼接
def make_seedurl(category, feature):
    dest_url = INDEX_URL.format(category=category, feature=feature)
    return dest_url

# 生成POST请求
def make_requests(seed_url):
    data = {
    "url": seed_url,
    "getway": "baitutrademark",
    "_sched_interval": 86400,
    "status": "OK",
    "priority": "4",
    "anti_cfg": {
        "proxy": True,
        "cookie": False,
        "random_ua": False
        }
    }
    dest_url = "http://127.0.0.1:8888/seeds"
    data = demjson.encode(data, encoding='utf-8')
    print(" make_request:", seed_url)
    response = requests.post(dest_url, data=data)
    return response

# 按国际分类生成该类下的所有种子
def generate_by_category(category):
    for feature in extract_features():
        seed_url = make_seedurl(category, feature)
        yield seed_url

# 指定生成 1.1.3 -- 1.11.3 之间的种子,所有国际分类的种子
def generate_by_feature(start, stop, seednum=None):
    counter = 0
    feature_list = list(extract_features())
    loop = False
    for feature in feature_list:
        if start == feature:
            loop = True
        if loop:
            for category in extract_category():
                if seednum and counter > seednum:
                    break
                seed_url = make_seedurl(category, feature)
                yield seed_url
                counter +=1
        if stop == feature:
            loop = False

# 生成指定数量的种子
def generate_part(start, stop):
    count = 0
    seed_urls = []
    for category in extract_category():
        for feature in extract_features():
            count += 1
            if count < start :
                continue
            seed_url = make_seedurl(category, feature)
            seed_urls.append(seed_url)
            if count >= stop :
                return seed_urls


if __name__ =="__main__":

    # 生成国际分类为1，全部特征的种子
    # url_seeds = list(generate_by_category(1))

    # 获取1.1.1 到1.1.5 之间的所有种子（国际分类 01 --45 ）===> 待修改
    # url_seeds = list(generate_by_feature("1.1.1","1.1.5"))

    # 上边的结果从前往后取10条
    # url_seeds = list(generate_by_feature("1.1.1", "1.1.5", 10))

    # 从 start 开始 到stop 结束的  从1开始 （1，10） 10个种子
    url_seeds = generate_part(1, 10)

    for url in url_seeds:
        make_requests(url)



